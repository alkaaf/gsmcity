package hoek.bubur.gsmcity;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by dalbo on 5/20/2017.
 */

public class C {
    public static final double MALANG_LAT = -7.983908;
    public static final double MALANG_LNG = 112.621391;
    public static final LatLng MALANG_LAT_LNG = new LatLng(MALANG_LAT, MALANG_LNG);
    public static final float DEF_ZOOM = 10;
}
